FROM node:15-alpine

# prepare
RUN mkdir /app /code
COPY ./package.json ./yarn.lock /app/
COPY ./package.json ./yarn.lock /code/

# install dependencies
WORKDIR /app
RUN yarn install --production

WORKDIR /code
RUN yarn install --production=false

# copy source
COPY ./src /code/src

# build
RUN yarn build

# copy dist
RUN cp -r dist /app

# clean
RUN rm -rf /code

# app
WORKDIR /app

# run command
CMD [ "node", "dist", "--seneca.log=level:warn" ]
