c = require 'centra'

{ IPFS_API = 'http://localhost:5001' } = process.env

bootstrap = ->
  seneca = require( 'seneca' )()
    .use 'transaction'
    .use 'util'
    .use 'crypto'
    .use 'tg'
    .use 'pubsub'
    .act 'role:pubsub,action:sub', {
      topic: 'tg'
      pattern: 'role:tg,action:update'
    }, process.stdout

apiLookup = ->

  c IPFS_API
    .send()
    .then ( res ) ->
      bootstrap()
    .catch ( err ) ->
      console.log 'looking for ipfs api'
      setTimeout apiLookup, 3000

apiLookup()
