c = require 'centra'

{ API_KEY } = process.env

module.exports = ( options ) ->

  seneca = @

  @add 'role:tg,action:update', ( msg, respond ) ->
    encodedMessage = Buffer.from( msg.data ).toString()

    @act 'role:transaction,action:serial', {
      __steps: [
        {
          __action: 'role:crypto,action:decode'
          encodedMessage
        }
        {
          __action: 'role:util,action:log'
          keyList: [ 'decodedMessage' ]
        }
        {
          __action: 'role:tg,action:echo'
        }
      ]
    }, respond

  @add 'role:tg,action:echo', ( msg, respond ) ->
    { message } = msg.decodedMessage

    @act 'role:tg,action:request', {
      methodName: 'sendMessage'
      data:
        chat_id: message.chat.id
        text: message.text
    }, respond

  @add 'role:tg,action:request', ( msg, respond ) ->
    respond null
    { methodName, data } = msg
    uri = "https://api.telegram.org/bot#{ API_KEY }/#{ methodName }"

    c uri, 'POST'
      .body data, 'json'
      .send()
      .then ( res ) ->
        console.log 'request successful'
        seneca.act 'role:transaction,action:success', {
          ...msg
          res
        }, process.stdout
      .catch ( err ) ->
        seneca.act 'role:transaction,action:error', {
          __pattern: 'role:tg,action:request'
          msg
          err
        }, process.stdout
